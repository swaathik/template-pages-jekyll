---
title: Learner Profile
---

Molly, the Molecular Biologist. PhD student, focused on wet lab experiments. 
Molly wants to compare her data with publicly available data but doesn't know how to retrieve and analyse the raw data available. 
Molly has some very basic background on programming, knows about basic Python (dictionaries, functions, lists), but no practical experience. 
Her aim is to publish her analysis results if it is possible.